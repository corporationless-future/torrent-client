import libtorrent as lt


class Torrent:
    '''Class for basic torrent operations'''
    states = [
        'queued', 'checking', 'downloading metadata',
        'downloading', 'finished', 'seeding',
        'allocating', 'checking fastresume'
    ]

    def __init__(self, tfile, path='.'):
        self.tfile = tfile
        self.path = path
        self.__init_params()

    def __init_params(self):
        info = lt.torrent_info(self.tfile)
        self.tparams = {
            'ti': info, 'save_path': self.path, 'auto_managed': False
        }

    def pause(self):
        self.handle.pause()

    def resume(self):
        self.handle.resume()

    def start(self):
        self.handle.resume()

    def get_status(self):
        if self.handle is None:
            return 'starting'
        s = self.handle.status()
        download_status = (
            '{:.2f}% complete (down: {:.1f}kB/s, up: {:.1f}kB/s, peers: {})'.
            format(
                s.progress * 100, s.download_rate / 1000,
                s.upload_rate / 1000, s.num_peers
                )
            )
        return '{}: {} \n {}'.format(
            self.handle.name(), self.states[s.state], download_status
        )
