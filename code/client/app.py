import libtorrent as lt
import sys
import time
import signal
from torrent import Torrent
from session import Session

if __name__ == '__main__':
    ses = Session(lt.session())

    ses.load_session()

    def signal_handler(*args):
        ses.save_session()
        sys.exit()
    signal.signal(signal.SIGINT, signal_handler)

    ses.listen(6881, 6891)
    for tfile in sys.argv[1:]:
        t = Torrent(tfile=tfile)
        ses.add(t)
        t.start()

    while True:
        time.sleep(1)
        for t in ses.torrents:
            print(t.get_status())
