import libtorrent as lt
import os
import pickle
import shutil
from torrent import Torrent


class Session:
    '''Wrapper class for lt.session '''
    LT_DEFAULT_ADD_TORRENT_FLAGS = (
        lt.add_torrent_params_flags_t.flag_paused |
        lt.add_torrent_params_flags_t.flag_auto_managed |
        lt.add_torrent_params_flags_t.flag_update_subscribe |
        lt.add_torrent_params_flags_t.flag_apply_ip_filter)

    def __init__(self, parent, sdir='.session'):
        self.__parent = parent
        self.torrents = []
        self.sdir = sdir
        self.__create_dir_for_save()

    def __create_dir_for_save(self):
        if not os.path.exists(self.sdir):
            os.makedirs(self.sdir)

    def listen(self, min_port, max_port):
        self.__parent.listen_on(min_port, max_port)

    def add(self, torrent):
        self.torrents.append(torrent)
        torrent.handle = self.__parent.add_torrent(torrent.tparams)
        shutil.copy(torrent.tfile, self.sdir)
        torrent.tfile = os.path.join(self.sdir, torrent.tfile)

    def remove(self, torrent, with_files=False):
        self.torrents.remove(torrent)
        self.__parent.remove_torrent(torrent.handle, with_files)
        self.__remove_state_and_torrent(torrent.tfile)

    def __remove_state_and_torrent(self, tfile):
        os.remove(tfile)
        os.remove(tfile + '.state')

    def save_session(self):
        for torrent in self.torrents:
            torrent.handle.save_resume_data()
            while True:
                self.__parent.wait_for_alert(10)
                a = self.__parent.pop_alert()
                if a is None:
                    fstate = os.path.join(sdir, torrent.tfile + '.state')
                    if os.path.isfile(fstate) is False:
                        os.remove(torrent.tfile)
                    break
                if a.what() == 'save_resume_data_alert':
                    self.__save_state(torrent, a.resume_data)
                    break

    def __save_state(self, torrent, resume_data):
        state = {
            'resume_data': resume_data,
            'save_path': torrent.path,
            'auto_managed': False
        }
        with open(torrent.tfile + '.state', 'wb') as f:
            pickle.dump(state, f)

    def load_session(self):
        for fname in os.listdir(self.sdir):
            if fname.endswith(".state"):
                tname = os.path.join(self.sdir, fname[:-(len('.state'))])
                if (os.path.isfile(tname)):
                    self.torrents.append(self.__load_torrent(fname, tname))

    def __load_torrent(self, fstate, ftorrent):
        with open(os.path.join(self.sdir, fstate), 'rb') as f:
            state = pickle.load(f)
        t = Torrent(ftorrent, state['save_path'])

        t.tparams['fast_resume'] = state['resume_data']
        t.tparams['flags'] = self.LT_DEFAULT_ADD_TORRENT_FLAGS | \
            lt.add_torrent_params_flags_t.flag_override_resume_data
        t.handle = self.__parent.add_torrent(t.tparams)
        t.start()
        return t
